CONDUIT_LIB_DIRS := /home/stefano/Work/MPI/OpenFPM/pyport/conduit/install-debug/lib
MODULE_NAME := pyport_module


clean:
	rm -rf build/
	rm .venv/lib/python3.9/site-packages/pyport*

init:
	export PYTHONPATH=/home/stefano/Work/MPI/OpenFPM/pyport/conduit/install-debug/python-modules
	export CONDUIT_DIR=/home/stefano/Work/MPI/OpenFPM/pyport/conduit/install-debug
	mkdir -p build/lib.linux-x86_64-3.9
	mkdir -p build/temp.linux-x86_64-3.9

compile:
	x86_64-linux-gnu-gcc \
		-pthread -Wno-unused-result -Wsign-compare \
		-DNDEBUG -fwrapv -O2 -Wall -g \
		-fPIC \
		-I/home/stefano/Work/MPI/OpenFPM/pyport/conduit/install-debug/include/conduit \
		-I/home/stefano/Work/MPI/OpenFPM/pyport/conduit/install-debug/python-modules/conduit \
		-I/home/stefano/Work/MPI/OpenFPM/pyport/.venv/include \
		-I/home/stefano/Work/MPI/OpenFPM/pybind11-2.6.2.tar/pybind11-2.6.2/include \
		-I/usr/include/python3.9 \
		-c ${MODULE_NAME}.cpp -o build/temp.linux-x86_64-3.9/${MODULE_NAME}.o \
		-std=c++11 -fPIC -shared


link:
	x86_64-linux-gnu-g++ \
		-pthread -shared -Wl,-Bsymbolic-functions,-z,relro -g -fwrapv -O2 \
		-L$(CONDUIT_LIB_DIRS) \
		build/temp.linux-x86_64-3.9/${MODULE_NAME}.o -o build/lib.linux-x86_64-3.9/${MODULE_NAME}$(shell python3-config --extension-suffix) \
		-lconduit -lconduit_blueprint -lconduit_relay

install:
	cp build/lib.linux-x86_64-3.9/${MODULE_NAME}$(shell python3-config --extension-suffix) .venv/lib/python3.9/site-packages/


# this is just to see what's actually going on inside `python setup.py install`
build:
	$(MAKE) init
	$(MAKE) compile
	$(MAKE) link
	$(MAKE) install


from_py:
	python setup.py install --force


benchmark:
	python benchmark.py
