import copy
import numpy as np


class World(object):
    """ A structure that holds the state of N bodies and
    additional variables.

    dt: (float) The time-step.
    N : (int) The number of bodies in the simulation.
    m : (1D ndarray) The mass of each body.
    r : (2D ndarray) The position of each body.
    v : (2D ndarray) The velocity of each body.
    F : (2D ndarray) The force on each body.

    """

    def __init__(self, N, m_min=1, m_max=30.0, r_max=50.0, v_max=4.0, dt=1e-3):
        self.N = N

        self.m = np.random.uniform(m_min, m_max, N)
        self.r = np.random.uniform(-r_max, r_max, (N, 2))
        self.v = np.random.uniform(-v_max, v_max, (N, 2))
        self.F = np.zeros_like(self.r)

        self.dt = dt

    def copy(self):
        return copy.deepcopy(self)

    def compute_F(self):
        """ Compute the force on each body in the world """

        for i in range(self.N):
            s = self.r - self.r[i]
            s3 = (s[:, 0] ** 2 + s[:, 1] ** 2) ** 1.5
            s3[i] = 1.0  # This makes the self-force zero.
            self.F[i] = (self.m[i] * self.m[:, None] * s / s3[:, None]).sum(0)

    def evolve(self, steps):
        """Evolve the world, through the given number of steps."""

        for _ in range(steps):
            self.compute_F()
            self.v += self.F * self.dt / self.m[:, None]
            self.r += self.v * self.dt

    def compute_F_numpy(self):
        """ Compute the force on each body in the world """

        for i in np.arange(self.N):
            s = self.r - self.r[i]
            s3 = np.power(
                np.square(s[:, 0]) + np.square(s[:, 1]), 1.5
            )
            s3[i] = 1.0  # This makes the self-force zero
            self.F[i] = (self.m[i] * self.m[:, None] * s / s3[:, None]).sum(0)

    def evolve_numpy(self, steps):
        """Evolve the world, through the given number of steps."""

        for _ in np.arange(steps):
            self.compute_F_numpy()
            self.v += self.F * self.dt / self.m[:, None]
            self.r += self.v * self.dt
