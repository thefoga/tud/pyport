import argparse
import numpy as np
from conduit import Node

from minimon import MiniMon
from common import World
import pyport_module as pyport


def py_style(world, steps, m):
    m.enter()

    world.evolve(steps)

    m.leave('python style')


def np_style(world, steps, m):
    m.enter()

    world.evolve_numpy(steps)

    m.leave('numpy style')


def cpp_style(world, steps, m):
    m.enter()

    pyport.evolve(
        world.dt,
        steps,
        world.N,
        world.m,
        world.r,
        world.v,
        world.F
    )

    m.leave('c++ style')


def conduit_style(world, steps, m):
    def insert_2d_array_in_node(n, arr, key):
        val_key = '{}/value'.format(key)
        shape_key = '{}/shape'.format(key)

        n[val_key] = arr
        n[shape_key] = arr.shape

    m.enter()

    n = Node()

    n['dt'] = world.dt
    n['steps'] = steps
    n['N'] = world.N

    insert_2d_array_in_node(n, world.m, 'm')
    insert_2d_array_in_node(n, world.r, 'r')
    insert_2d_array_in_node(n, world.v, 'v')
    insert_2d_array_in_node(n, world.F, 'F')

    pyport.conduit_evolve(n)

    m.leave('conduit style')


def numba_style(world, steps, m):
    m.enter()
    m.leave('numba style')


def pybind_style(world, steps, m):
    m.enter()

    pyport.evolve_pybind(
        world.dt,
        steps,
        world.N,
        world.m,
        world.r,
        world.v,
        world.F
    )

    print(world.r[2, 1])

    m.leave('pybind style')


def parse_args():
    parser = argparse.ArgumentParser()

    styles = 'py,np,c++,conduit,numba,pybind,all'.split(',')
    parser.add_argument(
        '--style', type=str, required=True, choices=styles
    )
    parser.add_argument(
        '--N', type=int, required=False, default=42, help='# bodies'
    )
    parser.add_argument(
        '--steps', type=int, required=False, default=1000, help='# steps'
    )

    return parser.parse_args()


def main():
    m = MiniMon()

    args = parse_args()
    world = World(args.N)

    if args.style == 'py':
        py_style(world, args.steps, m)
    elif args.style == 'np':
        np_style(world, args.steps, m)
    elif args.style == 'c++':
        cpp_style(world, args.steps, m)
    elif args.style == 'conduit':
        conduit_style(world, args.steps, m)
    elif args.style == 'numba':
        numba_style(world, args.steps, m)
    elif args.style == 'pybind':
        pybind_style(world, args.steps, m)
    elif args.style == 'all':
        # todo compare
        pass

    m.print_stats()


if __name__ == '__main__':
    main()
