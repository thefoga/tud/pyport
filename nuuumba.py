import numpy as np
from numba import stencil


def kernel(alpha):
    @stencil
    def _f(a):
        return \
            a[1, 0] +\
            a[-1, 0] +\
            a[0, 1] +\
            a[0, -1] +\
            alpha * a[0, 0]  # 3x3 kernel since I'm using -1 ... +1

    return _f


def lapl(dt, F):
    def _f(particles):
        return dt * np.power(particles, 3.0) +\
            dt * F * (particles - 1.0)

    return _f


def gray_scott(u, dt, F, alpha):
    def _f(particles):
        """ eq 2: x = x + u * ker(neighbourhood(x)) + lapl(x) """

        ker = u * kernel(alpha)(particles)
        l = lapl(dt, F)(particles)
        return ker + l

    return _f


def main():
    input_arr = np.arange(100).reshape((10, 10))

    pde = gray_scott(1.0, 1e-3, 1e-1, -4.0)
    output_arr = pde(input_arr)

    print(output_arr)


if __name__ == '__main__':
    main()
