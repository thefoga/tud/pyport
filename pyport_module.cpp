// todo does not work with numpy #define PY_SSIZE_T_CLEAN
#include <Python.h>

#include <numpy/arrayobject.h>

// use  proper strdup
#ifdef CONDUIT_PLATFORM_WINDOWS
    #define _conduit_strdup _strdup
#else
    #define _conduit_strdup strdup
#endif

#include "conduit.hpp"
#include "conduit_relay.hpp"
#include "conduit_blueprint.hpp"

// conduit python module capi header
#include "conduit_python.hpp"

// pybind
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
using pybind_array = pybind11::array_t<npy_float64>;

// general-purpose C++
#include <tuple>

// see https://github.com/johnnylee/python-numpy-c-extension-examples/blob/master/src/simple1.c
#define m(x0) (*(npy_float64*)((PyArray_DATA(py_m) +                \
                            (x0) * PyArray_STRIDE(py_m, 0))))
#define m_shape(i) (py_m->dimensions[(i)])

#define r(x1, x0) (*(npy_float64*)((PyArray_DATA(py_r) +                \
                                (x0) * PyArray_STRIDE(py_r, 0) +   \
                                (x1) * PyArray_STRIDE(py_r, 1))))
#define r_shape(i) (py_r->dimensions[(i)])

#define v(x1, x0) (*(npy_float64*)((PyArray_DATA(py_v) +                \
                                (x0) * PyArray_STRIDE(py_v, 0) +   \
                                (x1) * PyArray_STRIDE(py_v, 1))))
#define v_shape(i) (py_v->dimensions[(i)])

#define F(x1, x0) (*(npy_float64*)((PyArray_DATA(py_F) +              \
                                (x0) * PyArray_STRIDE(py_F, 0) +   \
                                (x1) * PyArray_STRIDE(py_F, 1))))
#define F_shape(i) (py_F->dimensions[(i)])


static inline void compute_F(npy_int64 N, PyArrayObject *py_m, PyArrayObject *py_r, PyArrayObject *py_F) {
    npy_int64 i, j;
    npy_float64 sx, sy, Fx, Fy, s3, tmp;

    // Set all forces to zero. 
    for(i = 0; i < N; ++i) {
        F(i, 0) = F(i, 1) = 0;
    }

    // Compute forces between pairs of bodies.
    for(i = 0; i < N; ++i) {
        for(j = i + 1; j < N; ++j) {
            sx = r(j, 0) - r(i, 0);
            sy = r(j, 1) - r(i, 1);

            s3 = sqrt(sx*sx + sy*sy);
            s3 *= s3 * s3;

            tmp = m(i) * m(j) / s3;
            Fx = tmp * sx;
            Fy = tmp * sy;

            F(i, 0) += Fx;
            F(i, 1) += Fy;

            F(j, 0) -= Fx;
            F(j, 1) -= Fy;
        }
    }
}


static inline void evolve(npy_float64 dt, npy_int64 steps, npy_int64 N, PyArrayObject *py_m, PyArrayObject *py_r, PyArrayObject *py_v, PyArrayObject *py_F) {
    for(npy_int64 step = 0; step < steps; ++step) {  // Evolve the world
        compute_F(N, py_m, py_r, py_F);

        for(npy_int64 i = 0; i < N; ++i) {
            v(i, 0) += F(i, 0) * dt / m(i);
            v(i, 1) += F(i, 1) * dt / m(i);
            
            r(i, 0) += v(i, 0) * dt;
            r(i, 1) += v(i, 1) * dt;
        }
    }
}


static PyObject* evolve_wrapper(PyObject *self, PyObject *args) {
    npy_int64 N, steps;
    npy_float64 dt;
    PyArrayObject *py_m, *py_r, *py_v, *py_F;

    if (!PyArg_ParseTuple(args, "dllOOOO", &dt, &steps, &N, &py_m, &py_r, &py_v, &py_F)) {
        return NULL;
    }

    evolve(dt, steps, N, py_m, py_r, py_v, py_F);

    Py_RETURN_NONE;
}


// #define USING_PYBIND
#ifdef USING_PYBIND
static inline npy_float64* pybind2ptr(pybind_array& a) {
    pybind11::buffer_info buf = a.request();
    return (npy_float64 *) buf.ptr;
}

static inline std::tuple<unsigned int, unsigned int> pybind2Dshape(pybind_array& a) {
    pybind11::buffer_info buf = a.request();
    unsigned int X = buf.shape[0];
    unsigned int Y = buf.shape[1];
    return std::make_tuple(X, Y);
}


// see https://stackoverflow.com/a/49693704/7643222
static void f(npy_float64 dt, npy_int64 steps, npy_int64 N, pybind_array m, pybind_array r, pybind_array v, pybind_array F) {
    auto ptr1 = pybind2ptr(r);
    auto shape = pybind2Dshape(r);
    // debug only std::cout << ptr1[2 * std::get<1>(shape) + 1] << std::endl;
    // todo `evolve`
}

PYBIND11_MODULE(pyport_module, m) {
    // todo import_array();
    // todo import_conduit();

    m.def("evolve_pybind", &f, "pybind.evolve_wrapper");
}

#else
static inline PyArrayObject* conduit_node2array(conduit::Node& node, int type) {
    const conduit::DataType& dtype = node.dtype();
    npy_intp len = (npy_intp) dtype.number_of_elements();
    void *data = node.element_ptr(0);
    std::cout << data << " ";
    return (PyArrayObject *) PyArray_SimpleNewFromData(1, &len, type, data);
}


static inline npy_float64 conduit_get_float_from_1d_array(conduit::Node& arr, npy_intp i) {
    return (npy_float64) arr.as_float64_array()[i];
}


static inline npy_float64 conduit_get_float_from_2d_array(conduit::Node& arr, npy_intp shape_columns, npy_intp coord_row, npy_intp coord_col) {
    npy_intp index = shape_columns * coord_row + coord_col;
    return (npy_float64) arr.as_float64_array()[index];
}


static inline void conduit_set_float_in_2d_array(conduit::Node& arr, npy_intp shape_columns, npy_intp coord_row, npy_intp coord_col, npy_float64 new_value) {
    npy_intp index = shape_columns * coord_row + coord_col;
    arr.as_float64_array()[index] = new_value;
}


static PyObject* conduit_evolve_wrapper(PyObject *self, PyObject *args) {
    PyObject *node;

    if(!PyArg_ParseTuple(args, "O", &node)) {
        return NULL;
    }

    if(!PyConduit_Node_Check(node)) {
        PyErr_SetString(PyExc_TypeError, "'argument must be a conduit.Node instance");

        return NULL;
    }

    conduit::Node& n = *PyConduit_Node_Get_Node_Ptr(node);

    // n.print();

    npy_int64 N = n["N"].value();
    npy_int64 steps = n["steps"].value();
    npy_float64 dt = n["dt"].value();

    auto v = n["v"]["value"];
    auto v_cols = n["v"]["shape"].as_int64_array()[1];
    std::cout << conduit_get_float_from_2d_array(v, v_cols, 3, 1) << std::endl;

    // evolve(dt, steps, N, py_m, py_m, py_m, py_m);

    Py_RETURN_NONE;
}


static PyMethodDef methods[] = {
    {"evolve", evolve_wrapper, METH_VARARGS, ""},
    {"conduit_evolve", (PyCFunction) conduit_evolve_wrapper, METH_VARARGS | METH_KEYWORDS, ""},
    {NULL, NULL, METH_VARARGS, NULL}
};


static struct PyModuleDef module = {
    PyModuleDef_HEAD_INIT,
    "pyport_module",  // name of extension module
    "",
    -1,  // size of per-interpreter state of the module, or -1 if the module keeps state in global variables.
    methods
};


PyMODINIT_FUNC PyInit_pyport_module(void) {
    import_array();
    import_conduit();
    return PyModule_Create(&module);
}

#endif
