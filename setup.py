from distutils.core import setup, Extension
from pathlib import Path

conduit_folder = Path().home() / 'scratch' / 'conduit' / 'install-debug'
conduit_includes = [
    'include/conduit',
    'python-modules/conduit'
]
conduit_includes = ' '.join(map(
    lambda x: '-I' + str(conduit_folder / x),
    conduit_includes
))
pybind_includes = '-I/home/stefano/Work/MPI/OpenFPM/pybind11-2.6.2.tar/pybind11-2.6.2/include'
compile_args = '-std=c++11 -fPIC -shared'  # -Wall -Wextra
extra_compile_args = ' '.join([
    conduit_includes,
    pybind_includes,
    compile_args
])

conduit_libs_folder = conduit_folder / 'lib'
conduit_libs = ' '.join(map(
    lambda x: '-l' + x,
    ['conduit', 'conduit_blueprint', 'conduit_relay']
))
extra_link_args = '-L' + str(conduit_libs_folder) + ' ' + conduit_libs

module = 'pyport'
module_fullname = '{}_module'.format(module)


def main():
    setup(
        name=module_fullname,
        version="0.0.1",
        description="pyport",
        author="sirfoga",
        author_email="stefano.fogarollo@tu-dresden.de",
        ext_modules=[
            Extension(
                module_fullname,
                sources=[
                    '{}.cpp'.format(module_fullname)
                ],
                extra_compile_args=extra_compile_args.split(),
                extra_link_args=extra_link_args.split(),
                language='c++'
            )
        ]
    )

if __name__ == "__main__":
    main()
